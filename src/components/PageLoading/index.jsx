// import { PageLoading } from '@ant-design/pro-layout'; // loading components from code split
// // https://umijs.org/plugin/umi-plugin-react.html#dynamicimport
//
// export default PageLoading;

import React from 'react';
import { Spin } from 'antd';

const PageLoading = () => {
  return (
    <div style={{ paddingTop: 100, textAlign: 'center' }}>
      <Spin size="large"  />
    </div>
  );
};

export default PageLoading;
