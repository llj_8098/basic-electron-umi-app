# Basic-Electron-Umi-App

This project is initialized with [Ant Design Pro](https://pro.ant.design). Follow is the quick guide for how to use.

## Environment Prepare

Install `node_modules`:

```bash
npm install
```

or

```bash
yarn
```

## Provided Scripts

### Start project

#### development

```bash
npm run electron-dev
```
#### production

```bash
npm run electron-start
```

### Build project

#### web应用打包（在./package.json中执行脚本）
```
npm run build
```
#### Windows（在./app/package.json中执行脚本）

##### 32位

```bash
npm run dist-win32
```
##### 64位

```bash
npm run dist-win64
```

#### Mac

```bash
npm run dist-mac
```

### Check code style

```bash
npm run lint
```

You can also use script to auto fix some lint error:

```bash
npm run lint:fix
```


## More
从零搭建Umi+Antd+Electron框架系列文章首发地址：

- 语雀：https://www.yuque.com/docs/share/a872bb18-fa3f-4059-aea0-be334d365451?# 《基于Umi搭建Electron App——从0到1》
- 语雀：https://www.yuque.com/docs/share/a7720474-5c95-4ccb-9a49-01e5c477c87f?# 《基于Umi搭建Electron App——打包优化》
